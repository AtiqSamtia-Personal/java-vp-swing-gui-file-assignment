
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class CourseModel {

    private HashMap<String, Course> list;

    public CourseModel() {
        this.list = new HashMap<String, Course>();
        load();
    }

    public void addCourse(String courseTitle, String courseCode,String creditHours) {
       
        if(courseCode.length() < 1){
            JOptionPane.showMessageDialog(null, "CourseCode Empty");
            return;
        }
        
        int credit = 0;
        try{
            credit = Integer.parseInt(creditHours);
        } catch(Exception e){
            
            JOptionPane.showMessageDialog(null, "Credit Hours should be a Number");
            return;
        }
        
        courseCode = courseCode.toUpperCase();
        if (!list.containsKey(courseCode)) {
            Course course = new Course(courseCode, courseTitle, credit);
            list.put(courseCode, course);
            JOptionPane.showMessageDialog(null, "Course Added");
        } else {
            JOptionPane.showMessageDialog(null, "Already Exist");
        }
    }

    public void deleteCourse(String CourseCode) {
        CourseCode = CourseCode.toUpperCase();
        if (list.containsKey(CourseCode)) {
            Course c = (Course) list.get(CourseCode);
            if (JOptionPane.showConfirmDialog(null, "Delete \n" + c + "\n? ", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                list.remove(CourseCode);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Course Not Found");
        }
    }

    public void searchCourse(String CourseCode) {
        CourseCode = CourseCode.toUpperCase();
        if (list.containsKey(CourseCode)) {
            Course c = (Course) list.get(CourseCode);
            c.display();
        } else {
            JOptionPane.showMessageDialog(null, "Course Not Found");
        }
    }

    public Course getCourse(String CourseCode) {
        CourseCode = CourseCode.toUpperCase();
        return list.get(CourseCode);
    }

    
    public void Filltable(DefaultTableModel model) {

         model.getDataVector().removeAllElements();

        int index = 1;
        for (String key : list.keySet()) {
            Course c = (Course) list.get(key);

            model.addRow(new Object[]{index + " ", c.getCourseTitle(), c.getCourseCode(),c.getCreditHours() + ""});

            index++;
        }
         model.fireTableDataChanged();
    }
    
    
    
     public void load() {

        String tokens[] = null;
        
        try {
            FileReader fr = new FileReader("course.txt");
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                tokens = line.split(",");
               
                Course c = new Course(tokens[0], tokens[1],Integer.parseInt(tokens[2]));
                list.put(tokens[0], c);
                line = br.readLine();
            }
            br.close();
            fr.close();
        } catch (IOException ioEx) {
            System.out.println(ioEx);
        }

    }

    public void save() {
        try {
            Course c;
            String line;
            FileWriter fw = new FileWriter("course.txt");
            PrintWriter pw = new PrintWriter(fw);

            for (String key : list.keySet()) {
                c = (Course) list.get(key);

                line = c.getCourseCode() + "," + c.getCourseTitle() + "," + c.getCreditHours();
                pw.println(line);

            }

            pw.flush();
            pw.close();
            fw.close();
        } catch (IOException ioEx) {
            System.out.println(ioEx);
        }
    }

    
}
