
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

 class RegistrationPanel implements ActionListener{

        
         JTabbedPane tab;
         
         JPanel panel,add,del,search,show,show_course,show_student;
         
         JButton b_add ,b_del,b_search,b_code,b_student;

         JTextField add_course,add_student,del_code,del_student,search_code,search_student,list_code,list_student;
         
         JTable table,table_student,table_course ;
         
         DefaultTableModel model,model_student,model_course;
       
        
        @Override
        public void actionPerformed(ActionEvent e) {
        
          Object source = e.getSource();
         
          
          if(source == b_add){
              
              Main.registration.addRegistration(add_student.getText(), add_course.getText());
               refresh(model);
              
          } else if(source == b_del){
              
              Main.registration.deleteRegistration(del_student.getText(),del_code.getText());
               refresh(model);
          } else if(source == b_search){
              Main.registration.searchRegistration(search_student.getText(),search_code.getText());
               refresh(model);
          }  else if(source == b_code){
           
               Main.registration.Filltable(model_course,list_code.getText());
          } else if(source == b_student){
           
              try{
              Main.registration.Filltable(model_student,Integer.parseInt(list_student.getText()));
              } catch(Exception ee){
                  JOptionPane.showMessageDialog(null, "Registration No is not a Number");
                  
              }
              
          }
          
        
        }
        
        
        public  RegistrationPanel(){
            
            // Course TAB Panel
            panel = new JPanel(new FlowLayout());
           
            tab = new JTabbedPane();
            
            
            // Add panel
             add = new JPanel(new GridLayout(20,2));
           
             add_student = new JTextField(20);
             add_course = new JTextField(20);
            
            b_add = new JButton("Add");
            b_add.addActionListener(this);
            
            
            JLabel l_name = new JLabel("Student ID : ");
            JLabel l_reg = new JLabel("CourseCode : ");
          
            add.add(l_name);
            add.add(add_student);
            add.add(l_reg);
            add.add(add_course);
            
            add.add(b_add);
            
            tab.addTab("Add",add);
            
            
            
            // Delete panel
             del = new JPanel(new GridLayout(20,2));
           
           
            // st_name = new JTextField(20);
             del_code = new JTextField(20);
             
             del_student = new JTextField(20);
            
            b_del = new JButton("Delete");
            b_del.addActionListener(this);
            
            JLabel l_name_ = new JLabel("Student ID : ");
            JLabel l_reg_ = new JLabel("CourseCode : ");
            del.add(l_name_);
            del.add(del_student);
            del.add(l_reg_);
            del.add(del_code);
            del.add(b_del);
            
            tab.addTab("Delete",del);
            
            
            
            // Search panel
             search = new JPanel(new GridLayout(20,2));
           
             
             
            // st_name = new JTextField(20);
             search_code = new JTextField(20);
             search_student = new JTextField(20);
            
            b_search = new JButton("Search");
            b_search.addActionListener(this);
            
            JLabel l_name__ = new JLabel("Student ID : ");
            JLabel l_reg__ = new JLabel("CourseCode : ");
            search.add(l_name__);
            search.add(search_student);
            search.add(l_reg__);
            search.add(search_code);
            search.add(b_search);
            
            tab.addTab("Search",search);
            
            
            
            show = new JPanel(new BorderLayout());
            
            model = new DefaultTableModel();
            table = new JTable(model); 
            
            model.addColumn("Sr."); 
            model.addColumn("RegNo");
            model.addColumn("CourseCode"); 
            
            refresh(model);
          
            JScrollPane scrollPane = new JScrollPane(table);
            
            show.add(scrollPane);
            
            tab.addTab("List",show);
            
            
            show_course = new JPanel(new BorderLayout());
            
            model_course = new DefaultTableModel();
            table_course = new JTable(model_course); 
            
            model_course.addColumn("Sr."); 
            model_course.addColumn("RegNo");
            model_course.addColumn("CourseCode"); 
            
         //   refresh(model_course);
          
            JScrollPane scrollPane_course = new JScrollPane(table_course);
            
            
            list_code = new JTextField(20);
            JLabel lc = new JLabel("Course Code");
            b_code = new JButton("List");
            b_code.addActionListener(this);
            
            
            JPanel jp = new JPanel(new FlowLayout());
            jp.add(lc);
            jp.add(list_code);
            jp.add(b_code);
            
            show_course.add(jp,BorderLayout.NORTH);
            show_course.add(scrollPane_course,BorderLayout.CENTER);
            
            tab.addTab("List by CourseCode",show_course);
            
            
            
            show_student = new JPanel(new BorderLayout());
            
            model_student = new DefaultTableModel();
            table_student = new JTable(model_student); 
            
            model_student.addColumn("Sr."); 
            model_student.addColumn("RegNo");
            model_student.addColumn("CourseCode"); 
            
        //    refresh(model_student);
          
            
            list_student = new JTextField(20);
            JLabel ls = new JLabel("Reg No");
            b_student = new JButton("List");
            b_student.addActionListener(this);
            
            
            JPanel jp2 = new JPanel(new FlowLayout());
            jp2.add(ls);
            jp2.add(list_student);
            jp2.add(b_student);
            
            show_student.add(jp2,BorderLayout.NORTH);
            
            
            
            JScrollPane scrollPane_stu = new JScrollPane(table_student);
            
            show_student.add(scrollPane_stu);
            
            tab.addTab("List by Student",show_student);
            
            
            panel.add(tab);
            
            
        }
        
        public JPanel getPanel(){
            return panel;
        }
        
         public void refresh(DefaultTableModel m){
              Main.registration.Filltable(m);
        }
        
    }
    