
public class Registration {
    private Student student;
    private Course course;
    public Registration(Student student, Course course) {
        this.student = student;
        this.course = course;
    }
    public Registration() {
        this.student = null;
        this.course = null;
    }
    public Student getStudent() {
        return student;
    }
    public void setStudent(Student student) {
        this.student = student;
    }
    public Course getCourse() {
        return course;
    }
    public void setCourse(Course course) {
        this.course = course;
    }
    public int getStudentRegistrationNo() {
        return this.student.getRegistrationNo();
    }
    public String getCourseCode() {
        return this.course.getCourseCode();
    }
    @Override
    public String toString() {
        return "RegNo : " + student.getRegistrationNo() + " - CourseCode : " + course.getCourseCode();
    }
    public void display() {
        System.out.println("Registration Info : \n" + this);
    }
}
