
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

 class StudentPanel implements ActionListener{

        
         JTabbedPane st_tab;
         
         JPanel st,add,del,search,show;
         
         JButton b_add ,b_del,b_search;

         JTextField add_name,add_reg,del_reg,search_reg;
         
         JTable table ;
         
         DefaultTableModel model;
       
        
        @Override
        public void actionPerformed(ActionEvent e) {
        
          Object source = e.getSource();
         
          
          if(source == b_add){
              
              Main.student.addStudent(add_reg.getText(), add_name.getText());
              refresh();
              
          } else if(source == b_del){
              
              Main.student.deleteStudent(del_reg.getText());
               refresh();
          } else if(source == b_search){
              Main.student.searchStudent(search_reg.getText());
               refresh();
          } 
         
          
        
        }
        
        
        public  StudentPanel(){
            
            // Student TAB Panel
            st = new JPanel(new FlowLayout());
           
            st_tab = new JTabbedPane();
            
            
            // Add panel
             add = new JPanel(new GridLayout(20,2));
           
             add_name = new JTextField(20);
             add_reg = new JTextField(20);
            
            b_add = new JButton("Add");
            b_add.addActionListener(this);
            
            
            JLabel l_name = new JLabel("Name : ");
            JLabel l_reg = new JLabel("Reg No : ");
            
            add.add(l_name);
            add.add(add_name);
            add.add(l_reg);
            add.add(add_reg);
            add.add(b_add,4);
            
            st_tab.addTab("Add",add);
            
            
            
            // Delete panel
             del = new JPanel(new FlowLayout());
           
            // st_name = new JTextField(20);
             del_reg = new JTextField(20);
            
            b_del = new JButton("Delete");
            b_del.addActionListener(this);
            
            JLabel l_name_ = new JLabel("Reg No : ");
            del.add(l_name_);
            del.add(del_reg);
            del.add(b_del);
            
            st_tab.addTab("Delete",del);
            
            
            
            // Delete panel
             search = new JPanel(new FlowLayout());
           
            // st_name = new JTextField(20);
             search_reg = new JTextField(5);
            
            b_search = new JButton("Search");
            b_search.addActionListener(this);
            
            JLabel l_name__ = new JLabel("Reg No : ");
            search.add(l_name__);
            search.add(search_reg);
            search.add(b_search);
            
            st_tab.addTab("Search",search);
            
            
            
            show = new JPanel(new BorderLayout());
            
            model = new DefaultTableModel();
            table = new JTable(model); 
            
            model.addColumn("Sr."); 
            model.addColumn("Name");
            model.addColumn("RegNo"); 
            
            
            refresh();
         
            JScrollPane scrollPane = new JScrollPane(table);
            
            show.add(scrollPane);
            
            
            st_tab.addTab("List",show);
            
            
            
            
            st.add(st_tab);
            
            
        }
        
        public JPanel getPanel(){
            return st;
        }
        
        public void refresh(){
              Main.student.Filltable(model);
        }
        
    }
    