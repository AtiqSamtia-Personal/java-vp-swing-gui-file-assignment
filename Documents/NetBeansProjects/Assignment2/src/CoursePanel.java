
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

 class CoursePanel implements ActionListener{

        
         JTabbedPane tab;
         
         JPanel panel,add,del,search,show;
         
         JButton b_add ,b_del,b_search;

         JTextField add_name,add_code,add_credit,del_code,search_code;
         
         JTable table ;
         
         DefaultTableModel model;
       
        
        @Override
        public void actionPerformed(ActionEvent e) {
        
          Object source = e.getSource();
         
          
          if(source == b_add){
              
              Main.course.addCourse(add_name.getText(), add_code.getText(),add_credit.getText());
               refresh();
              
          } else if(source == b_del){
              
              Main.course.deleteCourse(del_code.getText());
               refresh();
          } else if(source == b_search){
              Main.course.searchCourse(search_code.getText());
               refresh();
          } 
          
        
        }
        
        
        public  CoursePanel(){
            
            // Course TAB Panel
            panel = new JPanel(new FlowLayout());
           
            tab = new JTabbedPane();
            
            
            // Add panel
             add = new JPanel(new GridLayout(20, 2));
           
             add_name = new JTextField(20);
             add_code = new JTextField(10);
             add_credit = new JTextField(5);
            
            b_add = new JButton("Add");
            b_add.addActionListener(this);
            
            
            JLabel l_name = new JLabel("Name : ");
            JLabel l_reg = new JLabel("CourseCode : ");
            JLabel l_credit = new JLabel("CreditHours : ");
            
            add.add(l_name);
            add.add(add_name);
            add.add(l_reg);
            add.add(add_code);
            add.add(l_credit);
            add.add(add_credit);
            //add.add(null);
            add.add(b_add);
            
            tab.addTab("Add",add);
            
            
            
            // Delete panel
             del = new JPanel(new FlowLayout());
           
          
            // st_name = new JTextField(20);
             del_code = new JTextField(20);
            
            b_del = new JButton("Delete");
            b_del.addActionListener(this);
            
            JLabel l_name_ = new JLabel("CourseCode : ");
            del.add(l_name_);
            del.add(del_code);
            del.add(b_del);
            
            tab.addTab("Delete",del);
            
            
            
            // Search panel
             search = new JPanel(new FlowLayout());
           
             
             
            // st_name = new JTextField(20);
             search_code = new JTextField(20);
            
            b_search = new JButton("Search");
            b_search.addActionListener(this);
            
            JLabel l_name__ = new JLabel("CourseCode: ");
            search.add(l_name__);
            search.add(search_code);
            search.add(b_search);
            
            tab.addTab("Search",search);
            
            
            
            show = new JPanel(new BorderLayout());
            
            
            
            model = new DefaultTableModel();
            table = new JTable(model); 
            
            
            model.addColumn("Sr."); 
            model.addColumn("Name");
            model.addColumn("CourseCode"); 
            model.addColumn("CreditHours"); 
            
            
            refresh();
            
            JScrollPane scrollPane = new JScrollPane(table);
          
            show.add(scrollPane);
            
            
            tab.addTab("List",show);
            
            
            
            
            panel.add(tab);
            
            
        }
        
        public JPanel getPanel(){
            return panel;
        }
        
         public void refresh(){
              Main.course.Filltable(model);
        }
        
    }
    