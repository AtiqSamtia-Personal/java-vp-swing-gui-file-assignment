
import java.util.*;
import javax.swing.*;

public class Student {

    private int registrationNo;
    private String name;

    public Student() {
        registrationNo = 0;
        name = "";
    }

    public Student(int registrationNo, String name) {
        this();
        this.registrationNo = registrationNo;
        this.name = name;
    }

    public int getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(int registrationNo) {
        this.registrationNo = registrationNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "registrationNo: " + registrationNo + "\nname: " + name + "\n";
    }

    public void display() {
        JOptionPane.showMessageDialog(null, "Student Info : \n" + this);
    }
}
