
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

public class StudentModel {

    private HashMap<Integer, Student> list;

    public StudentModel() {
        this.list = new HashMap<Integer, Student>();
        load();
    }

    public void addStudent(String regNo, String name) {

        String Error = "";

        Integer r = 0;
        try {
            r = Integer.parseInt(regNo);
        } catch (Exception e) {

            Error = "Registration No is not a Number";
            JOptionPane.showMessageDialog(null, Error);
            return;
        }
        if (!list.containsKey(r)) {
            Student student = new Student(r, name);
            list.put(r, student);

            Error = "Student Added";

        } else {
            Error = "Already Exist";
        }

        JOptionPane.showMessageDialog(null, Error);
    }

    public void deleteStudent(String r) {

        String Error = "";

        Integer regNo = 0;
        try {
            regNo = Integer.parseInt(r);
        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, "Registration No is not a Number");
            return;
        }

        if (list.containsKey(regNo)) {
            Student s = (Student) list.get(regNo);
            if (JOptionPane.showConfirmDialog(null, "Delete \n" + s + "\n? ", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                list.remove(regNo);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Student Not Found");
        }
    }

    public void searchStudent(String r) {

        String Error = "";

        Integer regNo = 0;
        try {
            regNo = Integer.parseInt(r);
        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, "Registration No is not a Number");
            return;
        }

        if (list.containsKey(regNo)) {
            Student s = (Student) list.get(regNo);
            s.display();
        } else {
            JOptionPane.showMessageDialog(null, "Student Not Found");
        }
    }

    public Student getStudent(int regNo) {
        return list.get(regNo);
    }

    public void Filltable(DefaultTableModel model) {

        model.getDataVector().removeAllElements();

        int index = 1;
        for (int key : list.keySet()) {
            Student student = (Student) list.get(key);
            model.addRow(new Object[]{index + " ", student.getName(), student.getRegistrationNo()});
            index++;
        }
        model.fireTableDataChanged();
    }

    public void load() {

        String tokens[] = null;
      
        try {
            FileReader fr = new FileReader("student.txt");
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                tokens = line.split(",");
                int reg = Integer.parseInt(tokens[0]);
               
                Student s = new Student(reg, tokens[1]);
                list.put(reg, s);
                line = br.readLine();
            }
            br.close();
            fr.close();
        } catch (IOException ioEx) {
            System.out.println(ioEx);
        }

    }

    public void save() {
        try {
            Student s;
            String line;
            FileWriter fw = new FileWriter("student.txt");
            PrintWriter pw = new PrintWriter(fw);

            for (int key : list.keySet()) {
                s = (Student) list.get(key);

                line = s.getRegistrationNo() + "," + s.getName();
                pw.println(line);

            }

            pw.flush();
            pw.close();
            fw.close();
        } catch (IOException ioEx) {
            System.out.println(ioEx);
        }
    }

}
