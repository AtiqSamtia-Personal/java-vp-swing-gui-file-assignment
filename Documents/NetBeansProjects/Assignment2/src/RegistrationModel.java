

import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class RegistrationModel {

    private HashMap<String, Registration> list;

    public RegistrationModel() {
        this.list = new HashMap<String, Registration>();
        load();
    }

    public void addRegistration(String regNO,String courseCode) {
        
        courseCode = courseCode.toUpperCase();
        
        int reg = 0;
        try{
            reg = Integer.parseInt(regNO);
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, "Reg No is not a number");
            return;
        }
        Student s = Main.student.getStudent(Integer.parseInt(regNO));
        Course c = Main.course.getCourse(courseCode);
        if (s == null) {
            JOptionPane.showMessageDialog(null, "Student Does not exist");
            return;
        }
        if (c == null) {
            JOptionPane.showMessageDialog(null, "Course Does not exist");
            return;
        }
        String key = courseCode + "-" + regNO;
        if (!list.containsKey(key)) {
            Registration r = new Registration(s, c);
            list.put(key, r);
            JOptionPane.showMessageDialog(null, "Info Added");
        } else {
            JOptionPane.showMessageDialog(null, "Student cannot be allocated into one course Twice");
        }
    }

    public void deleteRegistration(String regNO, String CourseCode) {
        int regNo = 0;
        try{
            regNo = Integer.parseInt(regNO);
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, "Reg No is not a number");
            return;
        }
        
        CourseCode = CourseCode.toUpperCase();
        String key = CourseCode + "-" + regNo;
        if (list.containsKey(key)) {
            Registration r = (Registration) list.get(key);
            if (JOptionPane.showConfirmDialog(null, "Delete \n" + r + "\n? ", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                list.remove(key);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Registration Not Found");
        }
    }

    public void searchRegistration(String regNO, String CourseCode) {
        int regNo = 0;
        try{
            regNo = Integer.parseInt(regNO);
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, "Reg No is not a number");
            return;
        }
        CourseCode = CourseCode.toUpperCase();
        String key = CourseCode + "-" + regNo;
        Registration r = (Registration) list.get(key);
        if (list.containsKey(key)) {
            r.display();
        } else {
            JOptionPane.showMessageDialog(null, "Registration Not Found");
        }
    }
    
    
    public void Filltable(DefaultTableModel model) {

        model.getDataVector().removeAllElements();

        int index = 1;
        for (String key : list.keySet()) {
            Registration r = (Registration) list.get(key);
            model.addRow(new Object[]{index + " ", r.getStudentRegistrationNo(), r.getCourseCode()});
            index++;
        }
        model.fireTableDataChanged();
    }
    
    
    public void Filltable(DefaultTableModel model,int regNo) {

        
        model.getDataVector().removeAllElements();

        int index = 1;
        for (String key : list.keySet()) {
            Registration r = (Registration) list.get(key);
             if (r.getStudentRegistrationNo() == regNo) {
            model.addRow(new Object[]{index + " ", r.getStudentRegistrationNo(), r.getCourseCode()});
            index++;
        }
        }
        model.fireTableDataChanged();
    }
    
    
    
     public void Filltable(DefaultTableModel model,String courseCode) {

          courseCode = courseCode.toUpperCase();
        model.getDataVector().removeAllElements();

        int index = 1;
        for (String key : list.keySet()) {
            Registration r = (Registration) list.get(key);
            if (r.getCourseCode().equals(courseCode)) {
            model.addRow(new Object[]{index + " ", r.getStudentRegistrationNo(), r.getCourseCode()});
            index++;
        }
        }
        model.fireTableDataChanged();
    }
    
     
     
     
     
     
     
     
     
     public void load() {

        String tokens[] = null;
        
        try {
            FileReader fr = new FileReader("registrtion.txt");
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                tokens = line.split(",");
               
                 Student s = Main.student.getStudent(Integer.parseInt(tokens[0]));
                 Course c = Main.course.getCourse(tokens[1]);
       
                 String key = tokens[1] + "-" + tokens[0];
                
                Registration r = new Registration(s,c);
                list.put(key, r);
                line = br.readLine();
            }
            br.close();
            fr.close();
        } catch (IOException ioEx) {
            System.out.println(ioEx);
        }

    }

    public void save() {
        try {
            Registration r;
            String line;
            FileWriter fw = new FileWriter("registrtion.txt");
            PrintWriter pw = new PrintWriter(fw);

            for (String key : list.keySet()) {
                r = (Registration) list.get(key);

                line = r.getStudentRegistrationNo()+ "," + r.getCourseCode();
                pw.println(line);

            }

            pw.flush();
            pw.close();
            fw.close();
        } catch (IOException ioEx) {
            System.out.println(ioEx);
        }
    }

     

}
