
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

public class Main {

    public static StudentModel student;
    public static CourseModel course;
    public static RegistrationModel registration;

    public static void main(String[] args) {

        student = new StudentModel();
        course = new CourseModel();
        registration = new RegistrationModel();

        MainWindow mw = new MainWindow();

    }

    static class MainWindow extends WindowAdapter {

        JFrame fr;
        JTabbedPane tab, st_tab;

        JPanel st, co, re; // Student , Course , Registration tab

        public MainWindow() {

            fr = new JFrame("Main");
            fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            fr.addWindowListener(this);

            Container c = fr.getContentPane();

            tab = new JTabbedPane();

            StudentPanel ss = new StudentPanel();
            st = ss.getPanel();

            CoursePanel cc = new CoursePanel();
            co = cc.getPanel();

            RegistrationPanel rr = new RegistrationPanel();
            re = rr.getPanel();

            // Main tab Add Components
            tab.addTab("Student", st);
            tab.addTab("Course", co);
            tab.addTab("Registration", re);

            c.add(tab);

            fr.setSize(600, 700);
            fr.setVisible(true);

        }

        @Override
        public void windowClosing(WindowEvent we) {

            student.save();
            course.save();
            registration.save();

            System.exit(0);
        }

    }

}
